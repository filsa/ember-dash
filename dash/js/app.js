App = Ember.Application.create({});

App.Store = DS.Store.extend({
  revision: 12,
  adapter: 'DS.FixtureAdapter'
});

App.Router.map( function() {
  this.resource('links', function() {
    this.resource('link', {path: ':link_id'} )
  });
  this.resource("about");
});


App.Link = DS.Model.extend({
  url: DS.attr('string'),
  title: DS.attr('string'),
  tags: DS.attr('string'),
  created_at: DS.attr('date')

});

App.LinkController = Ember.ObjectController.extend({
  isEditing: false,
  edit: function(){
    console.log("inside edit");
    console.log(this);
    this.set('isEditing', true);
  },
  doneEditing: function(){
    this.set('isEditing', false);
  }
});

App.LinksController = Ember.ObjectController.extend({
  addLink: function(){
    App.Link.createRecord({
      title: 'New Link',
      url: 'http://',
      tags: '',
      created_at: new Date()
    });
  }

});

App.LinksRoute = Ember.Route.extend({
  model: function() {
    return App.Link.find();
  }
});


App.Link.FIXTURES = [
  {
    id: 1,
    url: "http://youtube.com/",
    title: "YouTube",
    tags: "video google",
    created_at: new Date('2013-03-15')
  },
  {
    id: 2,
    url: "http://www.vimeo.com/",
    title: "Vimeo",
    tags: "video artsy",
    created_at: new Date('2013-03-28')
  },
  {
    id: 3,
    url: "http://www.nytimes.com/",
    title: "NYT",
    tags: "news world",
    created_at: new Date('2013-03-28')
  },
  {
    id: 4,
    url: "http://www.eurogamer.net/",
    title: "Eurogamer",
    tags: "gaming news",
    created_at: new Date('2013-03-28')
  },
  {
    id: 5,
    url: "http://www.scripting.com/",
    title: "Scripting.com",
    tags: "news scripting geek",
    created_at: new Date('2013-03-28')
  },
  {
    id: 6,
    url: "http://www.twitch.tv",
    title: "Twitch",
    tags: "video gaming",
    created_at: new Date('2013-03-22')
  }
];

Ember.Handlebars.registerBoundHelper('date', function(date) {
  return moment(date).fromNow();
});
